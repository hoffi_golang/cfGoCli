package cli_test

import (
	"fmt"
	"log"
	"testing"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"github.com/onsi/ginkgo/reporters"
)

// VariablesForAllTests global variables for all tests in this suite
type VariablesForAllTests struct {
	RedisGUID         string
	RedisPlanGUID     string
	RedisInstanceGUID string
}

var SuiteGlobal = VariablesForAllTests{}

func TestCliPackage(t *testing.T) {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	RegisterFailHandler(Fail)
	junitReporter := reporters.NewJUnitReporter("junit.xml")
	RunSpecsWithDefaultAndCustomReporters(t, "cli Suite", []Reporter{junitReporter})
}

var _ = BeforeSuite(func() {

})

var _ = AfterSuite(func() {
	fmt.Printf("%+v\n", SuiteGlobal)
})
