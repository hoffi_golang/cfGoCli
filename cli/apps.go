package cli

import (
	"encoding/json"
	"log"
)

// Apps /v2/apps unmarshal json answer to given interface
func Apps(i interface{}) error {
	// get a http client from helpers.go
	client := GetClient()

	// prepare the request
	req, err := GetCcRequest("GET", "/v2/apps", nil)
	if err != nil {
		return err
	}

	// execute the request
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// parse the result
	jsonDecoder := json.NewDecoder(resp.Body)
	err = jsonDecoder.Decode(i)
	if err != nil {
		log.Fatal(err)
	}

	return nil
}
