package cli_test

import (
	"fmt"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/hoffi_golang/cfGoCli/cli"
	"gitlab.com/hoffi_golang/cfGoCli/config"
)

var _ = Describe("cf apps", func() {

	BeforeEach(func() {
		if config.Conf.BearerToken == "" {
			cli.SetConfigBearerToken()
		}
		if config.Conf.OrgID == "" {
			cli.SetConfigOrgID()
		}
		if config.Conf.SpaceID == "" {
			cli.SetConfigSpaceID()
		}
	})

	Describe("Apps querying", func() {
		Context("some org/space", func() {
			It("has some apps entries", func() {
				type jsonModelAppsCount struct {
					TotalResults int `json:"total_results"`
				}
				jsonModel := jsonModelAppsCount{}
				_ = cli.Apps(&jsonModel)
				fmt.Println("Nr. of Apps:", jsonModel.TotalResults)
				Expect(jsonModel.TotalResults > 0)
			})
		})
	})
})
