package cli

import (
	"crypto/tls"
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"net/http"

	"gitlab.com/hoffi_golang/cfGoCli/config"
)

// GetClient get a http client
func GetClient() *http.Client {
	client := http.DefaultClient
	if config.Conf.SkipSSL {
		tr := &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: config.Conf.SkipSSL},
		}
		client.Transport = tr
	}
	return client
}

// GetCcRequest prepare the request to CloudController
func GetCcRequest(httpMethod string, path string, bodyReader io.Reader) (*http.Request, error) {
	req, err := http.NewRequest(httpMethod, config.Conf.CcURL()+path, bodyReader)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Authorization", "Bearer "+config.Conf.BearerToken)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")
	return req, err
}

// DoCCHttpRequest to cloud controller api returns (response, http.status, error)
func DoCCHttpRequest(httpMethod string, path string, i interface{}, bodyReader io.Reader) (string, int, error) {
	// get a http client from helpers.go
	client := GetClient()

	// prepare the request
	req, err := GetCcRequest(httpMethod, path, bodyReader)
	if err != nil {
		return "", 0, err
	}

	verboseRequest(req)

	// execute the request
	resp, err := client.Do(req)
	if err != nil {
		return "", 0, err
	}
	defer resp.Body.Close()

	//verboseBody(resp.Body)

	// parse the result
	if i != nil {
		pipeReader, pipeWriter := io.Pipe()
		go func() {
			jsonDecoder := json.NewDecoder(io.TeeReader(resp.Body, pipeWriter))
			// it is important to close the writer or reading from the other end of the pipe will never finish
			defer pipeWriter.Close()
			err = jsonDecoder.Decode(i)
			if err != nil {
				log.Fatal(err)
			}
		}()
		bites, err1 := ioutil.ReadAll(pipeReader)
		if err != nil {
			log.Fatal(err1)
		}
		return string(bites), resp.StatusCode, nil
	}
	bites, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	return string(bites), resp.StatusCode, nil
}
