package cli

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"

	"gitlab.com/hoffi_golang/cfGoCli/config"
)

type jsonBearerModel struct {
	BearerToken string `json:"access_token"`
}

// SetConfigBearerToken config
func SetConfigBearerToken() (string, int, error) {
	client := http.DefaultClient
	if config.Conf.SkipSSL {
		tr := &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: config.Conf.SkipSSL},
		}
		client.Transport = tr
	}

	theBodyString := "username=" + config.Conf.User + "&password=" + config.Conf.Pass + "&client_id=cf&grant_type=password&response_type=token"
	theBody := strings.NewReader(theBodyString)
	req, err := http.NewRequest("POST", config.Conf.UaaURL()+"/oauth/token", theBody)
	if err != nil {
		log.Fatal(err)
		return "", -1, err
	}
	req.SetBasicAuth("cf", "")
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	if config.Conf.Verbose {
		fmt.Println("req:", req, "\ndata:", theBodyString)
	}

	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
		return "", -1, err
	}
	defer resp.Body.Close()

	if config.Conf.Verbose {
		fmt.Println("statusCode:", resp.StatusCode)
	}

	jsonDecoder := json.NewDecoder(resp.Body)
	jsonModel := jsonBearerModel{}
	err = jsonDecoder.Decode(&jsonModel)
	if err != nil {
		log.Fatal(err)
		return "", -1, err
	}

	// contents, err := ioutil.ReadAll(resp.Body)
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// s := string(contents)
	// fmt.Println(s)

	if config.Conf.Verbose {
		header := resp.Header
		for key, value := range header {
			fmt.Println("   ", key, ":", value)
		}
	}

	config.Conf.BearerToken = jsonModel.BearerToken

	return jsonModel.BearerToken, resp.StatusCode, nil
}
