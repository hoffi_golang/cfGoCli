package cli_test

import (
	"errors"
	"log"
	"regexp"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/hoffi_golang/cfGoCli/cli"
	"gitlab.com/hoffi_golang/cfGoCli/config"
)

type Resource struct {
	Metadata struct {
		GUID string `json:"guid"`
	}
	Entity struct {
		Name  string `json:"name,omitempty"`
		Label string `json:"label,omitempty"`
	}
}
type jsonModel struct {
	TotalResults int `json:"total_results"`
	Resources    []Resource
}

var _ = Describe("cf marketplace", func() {

	BeforeEach(func() {
		if config.Conf.BearerToken == "" {
			cli.SetConfigBearerToken()
		}
		if config.Conf.OrgID == "" {
			cli.SetConfigOrgID()
		}
		if config.Conf.SpaceID == "" {
			cli.SetConfigSpaceID()
		}
	})

	Describe("Marketplace querying", func() {
		Context("the space", func() {
			It("has some services entries", func() {
				model := jsonModel{}
				_, status, err := cli.MarketplaceServices(&model)
				if err != nil {
					log.Fatal(err)
				}
				cli.VerboseStruct(model)

				Expect(status).To(BeNumerically("==", 200))
				Expect(model.TotalResults).To(BeNumerically(">", 0))
				Expect(len(model.Resources)).To(BeNumerically(">", 0))
			})
		})
	})

	Describe("Marketplace querying", func() {
		Context("the space", func() {
			It("exists an keyvalue service", func() {
				model := jsonModel{}
				_, status, err := cli.MarketplaceServices(&model)
				if err != nil {
					log.Fatal(err)
				}
				cli.VerboseStruct(model)

				//regex := regexp.MustCompile("^(p-rabbitmq|cloudamqp)")
				regex := regexp.MustCompile("^(p-redis|rediscloud)")
				matcher := func(r Resource) bool {
					return regex.MatchString(r.Entity.Label)
				}
				resource, err := Any(model.Resources, matcher)
				if err != nil {
					log.Fatal(err)
				}
				Expect(resource.Entity.Label).To(MatchRegexp("^(p-redis|rediscloud)"))
				SuiteGlobal.RedisGUID = resource.Metadata.GUID
				Expect(status).To(BeNumerically("==", 200))
			})
		})
	})

	Describe("Marketplace querying", func() {
		Context("the space", func() {
			It("has a known redis service plan", func() {
				model := jsonModel{}
				_, status, err := cli.MarketplaceServicePlans(SuiteGlobal.RedisGUID, &model)
				if err != nil {
					log.Fatal(err)
				}
				cli.VerboseStruct(model)

				//regex := regexp.MustCompile("^(p-rabbitmq|cloudamqp)")
				regex := regexp.MustCompile("^(shared-vm|30mb)")
				matcher := func(r Resource) bool {
					return regex.MatchString(r.Entity.Name)
				}
				resource, err := Any(model.Resources, matcher)
				if err != nil {
					log.Fatal(err)
				}
				Expect(resource.Entity.Name).To(MatchRegexp("^(shared-vm|30mb)"))
				SuiteGlobal.RedisPlanGUID = resource.Metadata.GUID
				Expect(status).To(BeNumerically("==", 200))
			})
		})
	})

	Describe("Marketplace create", func() {
		Context("service instance in space", func() {
			It("redis or rediscloud", func() {
				model := Resource{}
				_, status, err := cli.MarketplaceCreateService("uat-redisinstance1", config.Conf.SpaceID, SuiteGlobal.RedisPlanGUID, &model)
				if err != nil {
					log.Fatal(err)
				}
				cli.VerboseStruct(model)

				Expect(status).To(BeNumerically("==", 201))
				SuiteGlobal.RedisInstanceGUID = model.Metadata.GUID
			})
		})
	})

	Describe("Marketplace query", func() {
		Context("service instance in space", func() {
			It("redis or rediscloud", func() {
				model := Resource{}
				_, status, err := cli.MarketplaceServiceInstance(SuiteGlobal.RedisInstanceGUID, &model)
				if err != nil {
					log.Fatal(err)
				}
				cli.VerboseStruct(model)

				Expect(status).To(BeNumerically("==", 200))
				Expect(model.Entity.Name).To(Equal("uat-redisinstance1"))
			})
		})
	})

	Describe("Marketplace delete", func() {
		Context("service instance in space", func() {
			It("redis or rediscloud", func() {
				_, status, err := cli.MarketplaceDeleteService(SuiteGlobal.RedisInstanceGUID)
				if err != nil {
					log.Fatal(err)
				}

				Expect(status).To(BeNumerically("==", 204))
			})
		})
	})

})

func Any(resources []Resource, match func(r Resource) bool) (Resource, error) {
	// fmt.Printf("%+v\n", resources)
	for _, r := range resources {
		if match(r) {
			return r, nil
		}
	}
	return Resource{}, errors.New("array does not satisfy condition")
}
