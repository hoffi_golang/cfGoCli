package cli

import (
	"fmt"
	"io"
	"net/http"

	"gitlab.com/hoffi_golang/cfGoCli/config"
)

// VerboseStruct print with keys the given struct
func VerboseStruct(i interface{}) {
	if config.Conf.Verbose {
		fmt.Printf("%+v\n", i)
	}
}

func verboseRequest(req *http.Request) {
	if config.Conf.Verbose {
		fmt.Println("http.Request:", req)
	}
}

func verboseBody(readcloser io.ReadCloser) {
	if config.Conf.Verbose {
		fmt.Println("response.Body:", readcloser)
	}
}

func verbosePrintln(s string) {
	if config.Conf.Verbose {
		fmt.Println(s)
	}
}
