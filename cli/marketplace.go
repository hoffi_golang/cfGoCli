package cli

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/hoffi_golang/cfGoCli/config"
)

// MarketplaceServices /v2/spaces/SPACEGUID/services unmarshal json answer to given interface
func MarketplaceServices(i interface{}) (string, int, error) {
	// prepare the request
	path := fmt.Sprintf("/v2/spaces/%v/services", config.Conf.SpaceID)
	response, status, err := DoCCHttpRequest("GET", path, i, nil)
	if err != nil {
		return "", 0, err
	}
	verbosePrintln(response)
	verbosePrintln(strconv.Itoa(status))

	return response, status, err
}

// MarketplaceServicePlans /v2/service_plans?q=service_guid:SERVICEGUID unmarshal json answer to given interface
func MarketplaceServicePlans(serviceGUID string, i interface{}) (string, int, error) {
	// prepare the request
	path := fmt.Sprintf("/v2/service_plans?q=service_guid:%v", serviceGUID)
	response, status, err := DoCCHttpRequest("GET", path, i, nil)
	if err != nil {
		return "", 0, err
	}
	verbosePrintln(response)
	verbosePrintln(strconv.Itoa(status))

	return response, status, err
}

// MarketplaceCreateService /v2/service_instances unmarshal json answer to given interface
func MarketplaceCreateService(instanceName string, spaceGUID string, servicePlanGUID string, i interface{}) (string, int, error) {
	// prepare the request
	theBodyString := fmt.Sprintf("{\"name\":\"%v\",\"space_guid\":\"%v\",\"service_plan_guid\":\"%v\"}", instanceName, spaceGUID, servicePlanGUID)
	theBodyReader := strings.NewReader(theBodyString)
	response, status, err := DoCCHttpRequest("POST", "/v2/service_instances", i, theBodyReader)
	if err != nil {
		return "", 0, err
	}
	verbosePrintln(response)
	verbosePrintln(strconv.Itoa(status))

	return response, status, nil
}

// MarketplaceServiceInstance /v2/service_instances
func MarketplaceServiceInstance(instanceGUID string, i interface{}) (string, int, error) {
	// prepare the request
	path := fmt.Sprintf("/v2/service_instances/%v", instanceGUID)
	response, status, err := DoCCHttpRequest("GET", path, i, nil)
	if err != nil {
		return "", 0, err
	}
	verbosePrintln(response)
	verbosePrintln(strconv.Itoa(status))

	return response, status, nil
}

// MarketplaceDeleteService /v2/service_instances
func MarketplaceDeleteService(instanceGUID string) (string, int, error) {
	// prepare the request
	path := fmt.Sprintf("/v2/service_instances/%v", instanceGUID)
	response, status, err := DoCCHttpRequest("DELETE", path, nil, nil)
	if err != nil {
		return "", 0, err
	}
	verbosePrintln(response)
	verbosePrintln(strconv.Itoa(status))

	return response, status, nil
}
