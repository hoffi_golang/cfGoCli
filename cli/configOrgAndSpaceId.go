package cli

import (
	"fmt"
	"log"

	"gitlab.com/hoffi_golang/cfGoCli/config"
)

// SetConfigOrgID set OrgId in config.Conf
func SetConfigOrgID() {
	type jsonModel struct {
		TotalResults int `json:"total_results"`
		Resources    []struct {
			Metadata struct {
				GUID string `json:"guid"`
			}
			Entity struct {
				Name string `json:"name"`
			}
		}
	}
	model := jsonModel{}
	_, _, err := OrgByName(&model)
	if err != nil {
		log.Fatal(err)
	}
	config.Conf.OrgID = model.Resources[0].Metadata.GUID
}

// SetConfigSpaceID set SpaceID in config.Conf
func SetConfigSpaceID() {
	type jsonModel struct {
		TotalResults int `json:"total_results"`
		Resources    []struct {
			Metadata struct {
				GUID string `json:"guid"`
			}
			Entity struct {
				Name string `json:"name"`
			}
		}
	}
	model := jsonModel{}
	_, _, err := SpaceByName(&model)
	if err != nil {
		log.Fatal(err)
	}
	config.Conf.SpaceID = model.Resources[0].Metadata.GUID
}

// OrgByName /v2/organizations?q=name:%v unmarshal json answer to given interface
func OrgByName(i interface{}) (string, int, error) {
	// prepare the request
	path := fmt.Sprintf("/v2/organizations?q=name:%v&inline-relations-depth=1", config.Conf.ORG)
	response, status, err := DoCCHttpRequest("GET", path, i, nil)
	if err != nil {
		return "", 0, err
	}
	verbosePrintln(response)

	return response, status, nil
}

// SpaceByName /v2/organizations?q=name:%v unmarshal json answer to given interface
func SpaceByName(i interface{}) (string, int, error) {
	// prepare the request
	path := fmt.Sprintf("/v2/organizations/%v/spaces?q=name:%v&inline-relations-depth=1", config.Conf.OrgID, config.Conf.Space)
	response, status, err := DoCCHttpRequest("GET", path, i, nil)
	if err != nil {
		return "", 0, err
	}
	verbosePrintln(response)

	return response, status, nil
}
