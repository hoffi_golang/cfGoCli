package main

import (
	"fmt"
	"log"

	"gitlab.com/hoffi_golang/cfGoCli/cli"
	"gitlab.com/hoffi_golang/cfGoCli/config"
)

// Resource for cc json answer Resource element
type Resource struct {
	Metadata struct {
		GUID string `json:"guid"`
	}
	Entity struct {
		Label string `json:"label"`
	}
}
type jsonModel struct {
	TotalResults int `json:"total_results"`
	Resources    []Resource
}

// instead of running this is considered to be pure test
// so run with $ go test ./...
// or $ go test gitlab.com/hoffi_golang/cfGoCli/cli
func main() {
	_, c, _ := cli.SetConfigBearerToken()
	fmt.Println("\n======== Main.go BearerToken ============")
	fmt.Println("statusCode: ", c)
	fmt.Println(config.Conf.BearerToken)

	// fmt.Println("======== Main.go Apps ===================")
	// type jsonModelAppsCount struct {
	// 	TotalResults int `json:"total_results"`
	// }
	// jsonModel := jsonModelAppsCount{}
	// _ = cli.Apps(&jsonModel)
	// fmt.Println("Nr. of Apps:", jsonModel.TotalResults)

	fmt.Println("\n======== Main.go OrgID ====================")
	cli.SetConfigOrgID()
	fmt.Println("config.Conf.OrgID:", config.Conf.OrgID)
	fmt.Println("\n======== Main.go SpaceID ==================")
	cli.SetConfigSpaceID()
	fmt.Println("config.Conf.SpaceID:", config.Conf.SpaceID)
	fmt.Println("\n======== Main.go Marketplace ==============")
	model := jsonModel{}
	_, _, err := cli.MarketplaceServices(&model)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(model)
}
