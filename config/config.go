package config

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
)

// Conf parameters instance
var Conf = new(Config)

// Config for pcf target
type Config struct {
	BearerToken string `json:"-"`
	User        string `json:"-"` // read from CF_USERNAME
	Pass        string `json:"-"` // read from CF_PASSWORD

	HTTP    string `json:"http,omitempty"`
	BaseURL string `json:"baseURL,omitempty"`
	SkipSSL bool   `json:"skipSSL,omitempty"` // "--skip-ssl-validation"
	Verbose bool   `json:"verbose,omitempty"` // "-v" or "-s"
	ORG     string `json:"ORG,omitempty"`
	OrgID   string `json:"-"`
	Space   string `json:"Space,omitempty"`
	SpaceID string `json:"-"`
}

// CcURL url of cloudcontroller
func (c Config) CcURL() string {
	return c.HTTP + "api." + c.BaseURL

}

// UaaURL url of user authentification and authorization
func (c Config) UaaURL() string {
	return c.HTTP + "uaa." + c.BaseURL

}

// Constructor
func init() {
	// deciding where config.json lies
	dir, exists := os.LookupEnv("APPHOME_CURRENT")
	if exists {
		fmt.Println("reading config.json from APPHOME_CURRENT", dir+"/config")
	} else {
		currDir, err := filepath.Abs(filepath.Dir(os.Args[0]))
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println("reading config.json from ", currDir+"/config")
		dir = currDir
	}

	// reading config.json
	configFilename := dir + "/config/config.json"
	fmt.Println("reading", configFilename)
	file, err := os.Open(configFilename)
	if err != nil {
		log.Fatal("cannot open ", configFilename)
		log.Fatal("error:", err)
	}
	decoder := json.NewDecoder(file)
	err = decoder.Decode(&Conf)
	if err != nil {
		log.Fatal("error:", err)
	}

	// ---------------------------------------------------------------------------
	// ------ reading username and password from system env variables ------------
	// ---------------------------------------------------------------------------
	user := os.Getenv("CF_USERNAME")
	pass := os.Getenv("CF_PASSWORD")
	if user == "" {
		log.Fatal("CF_USERNAME not set")
	} else {
		fmt.Println("read user from CF_USERNAME")
	}
	if pass == "" {
		log.Fatal("CF_PASSWORD is empty")
	} else {
		fmt.Println("read pass from CF_PASSWORD")
	}

	Conf.User = user
	Conf.Pass = pass
}
